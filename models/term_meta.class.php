<?php


class TermMeta extends DbConnect{
        private  $table = 'term_meta';
        
    

    public function __construct(){

        parent::__construct();
    }

    public function add( $name, $taxonomy, $parent_id, $remote_id=null ){
            $this->db->insert( $this->table,[
                         'name'=> $name,
                         'taxonomy'=> $value,
                         'parent_id'=> ( int ) $user_id,
                         'remote_id'=> $remote_id
                        
                ]); 
            return $this->db->id() > 0 ? true:  false ;
    }

    public  function delete( $id ){
            $delete = $this->db->delete( $this->table, [
                        'id'=>$id
                            ]);
            
            return $delete > 0 ? true: false;
    }

    //update posts table
    public function update( $id, $field, $newvalue ){
           $update = $this->db->update( $this->table, [
                                          $field => $newvalue ],
                                           [
                                             'id' => $id 
                                            ] );
            return $update > 0 ? true : false;
          
    }

    
    //select records from table
    public function get( $identifier=null ){
         if( ( int )$identifier ):
            $results = $this->db->select( $this->table,[ 'id', 'name', 'taxonomy', 'parent_id', 'remote_id'],
                                                                        ['id'=> $identifier, 'LIMIT'=>'1'] );

            return !empty( $results ) ? json_encode( $results[0] ) : [];
         else:
            $results = $this->db->select( $this->table, '*');

            return !empty( $results ) ? json_encode( $results ) : [];            
        endif;   
    }



    //pagination
    public function get_records( $page,$per_page ){

        //pagination object
        $pagination = ( object ) [
                'per_page'=> isset( $per_page ) ? ( int ) $per_page : 4,
                'page' => isset( $page ) && ($page > 0 ) ? ( int ) $page : 1
           ];

        //limit
         $limit = ( object )[ 
                        'limit_1' => ( $pagination->page - 1 ) * $pagination->per_page, 
                        'limit_2' => $pagination->per_page
                 ];

        
         $results =  $this->db->select( $this->table, 
                                [ 'id', 'name', 'taxonomy', 'parent_id', 'remote_id'],
                                ['LIMIT'=> [$limit->limit_1, $limit->limit_2 ] ] ); 

                return isset( $results ) ? json_encode( $results ): [];  
    
        
    }


}